# Bug Exploration Zustand and ESM

In this repo I am just exploring a bug that I ran into when using [zustand] and type=module
in the package.json.
Maybe it all turns out to just have been a problem of mine, if so, great. 
If not I will open an issue or even better a PR in the according project.

Update: since zustand 3.4.1 the problems are different.

## The Problem

In another project I am using zustand together with mocha, in a pure ESM setup,
no transpilation with babel or alike. Why? Because it's 2021! Seriously.
Why use the intermediate steps if the future is no (or less) transpilation, the 
JS engines evolve so fast, let's use it as is.  
I configured in the package.json the option `type=module`, which makes this 
a "modern" ESM based project.

In the [nodejs docs](https://nodejs.org/api/packages.html#packages_determining_module_system) 
one can read what the means for a project, how files are imported and 
which file must or can contain `require` or `import` to load other files.
It's not trivial.

I wanted to get zustand to be testable with mocha. But I ran into:
1) When using `type=module` and importing `zustand/vanilla` mocha says
   there is no such file. No idea what that might be.
   See the [vanilla.test.js](./src/vanilla.test.js).
   **UPDATE: this works since zustand 3.4.1**
2) When using `type=module` and importing `zustand/vanilla` it can't find
   the `create` function, somehow the export is broken.
   When changing the export inside the `zustand/vanilla` from   
   `exports.default = create;`  
   to   
   `module.exports = create;`  
   I can import the file. This sounds like a packager problem in zustand?
   See the [vanilla-create.test.js](src/vanilla-create.test.js).
   I am wondering why not `node_modules/zustand/esm/vanilla.js` is imported,
   but `node_modules/zustand/vanilla.js`.
   
[zustand]: https://github.com/pmndrs/zustand

## A Solution

With this [pull request for zustand](https://github.com/pmndrs/zustand/pull/336)
I suggest a possible solution, which generates all files as pure ESM
modules, so importing them in a ESM-only package should not cause problems.
See the [vanilla-esm.test.js](./src/vanilla-esm.test.js).


## Use this Repo

In order to set up a reproducable environment use the following:
- `docker-compose up -d` starts the docker container with the nodejs setup inside
- `docker-compose exec node bash` open the shell *inside* the container
- now there is `node` available, the environment set up for running this code

- `npm install` installs all the dependencies this projects needs
- `npm test` runs the test

To run each test file to see the separate results which have different
causes and problems like so:
- `node src/vanilla.test.js` fails due to not finding the file, a bit strange imho, see explanation above
- `node src/vanilla-js.test.js` fails because the export is wrong, I would say

See each file for some details.
