import {strict as assert} from 'assert';

// This uses the code produced by https://github.com/pmndrs/zustand/pull/336
// This solves the importing into a pure ESM package.
import create from '../zustand-esm/esm/vanilla.js';

// the imported `create` exists
assert(create);

// the imported `create` is a function
assert.equal(typeof create, 'function');

/*
Tests pass ... yeah
No output is same as pass, if it failed exception would be thrown.

# node src/vanilla-esm.test.js



 */