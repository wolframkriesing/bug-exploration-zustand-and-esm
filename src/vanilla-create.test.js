import {strict as assert} from 'assert';
import create from 'zustand/vanilla';

// the imported `create` exists
assert(create);

// test that the imported `create` is a function
assert.equal(typeof create, 'function');

// The last test fails, due to `zustand/vanilla.js` exporting `create`
// like this `exports.default = create;`
// if it was `module.exports = create;` it would work :(


/*

# node src/vanilla-create.test.js
internal/modules/run_main.js:54
    internalBinding('errors').triggerUncaughtException(
                              ^

AssertionError [ERR_ASSERTION]: Expected values to be strictly equal:
+ actual - expected

+ 'object'
- 'function'
    at file:///app/src/vanilla-create.test.js:8:8
    at ModuleJob.run (internal/modules/esm/module_job.js:110:37)
    at async Loader.import (internal/modules/esm/loader.js:179:24) {
  generatedMessage: true,
  code: 'ERR_ASSERTION',
  actual: 'object',
  expected: 'function',
  operator: 'strictEqual'
}

 */